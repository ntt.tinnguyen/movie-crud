package com.aavn.vn.crud.service;

import com.aavn.vn.crud.CrudApplicationTests;
import com.aavn.vn.crud.entity.Category;
import com.aavn.vn.crud.exception.CategoryNotFoundException;
import com.aavn.vn.crud.model.CategoryDTO;
import com.aavn.vn.crud.repository.CategoryRepository;
import com.aavn.vn.crud.service.serviceImpl.CategoryServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = CrudApplicationTests.class)
public class CategoryServiceImplTest {
    @InjectMocks
    CategoryServiceImpl categoryService;

    @Mock
    CategoryRepository categoryRepository;

    private Category category;
    private List<Category> categoryList;

    @Before
    public void createCategory(){
        category = new Category(1,"Action");
    }

    @Before
    public void createCategoryList(){
        categoryList = new ArrayList<>();
        categoryList.add(new Category(1, "Action"));
        categoryList.add(new Category(2, "Sci-fi"));
        categoryList.add(new Category(3, "Comedy"));
    }

    @Test
    public void whenGetAllCategory_thenReturnListCategory(){
        when(categoryRepository.findAll()).thenReturn(categoryList);
        List<CategoryDTO> categoryDTOList = categoryService.getListCategoryDTO();
        assertEquals(categoryDTOList.size(), categoryList.size());
    }

    @Test
    public void whenGetCategory_withExistCategory_thenReturnCategory(){
        when(categoryRepository.findCategoryByName(any(String.class))).thenReturn(java.util.Optional.ofNullable(category));
        Category category_entity = categoryService.findCategoryByName("Action");
        assertEquals(category_entity.getId(), category.getId());
    }

    @Test(expected = CategoryNotFoundException.class)
    public void whenGetCategory_withNotExistCategory_thenReturnError(){
        when(categoryRepository.findCategoryByName(any(String.class))).thenThrow(CategoryNotFoundException.class);
        categoryService.findCategoryByName("Invalid Name");
    }
}

package com.aavn.vn.crud.service;

import com.aavn.vn.crud.CrudApplicationTests;
import com.aavn.vn.crud.entity.Nation;
import com.aavn.vn.crud.exception.NationNotFoundException;
import com.aavn.vn.crud.model.NationDTO;
import com.aavn.vn.crud.repository.NationRepository;
import com.aavn.vn.crud.service.serviceImpl.NationServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = CrudApplicationTests.class)
public class NationServiceImplTest {
    @InjectMocks
    NationServiceImpl nationService;

    @Mock
    NationRepository nationRepository;

    private static Nation nation;
    List<Nation> nations;

    @Before
    public void createNation(){
        nation = new Nation(1,"Viet Nam");
    }

    @Before
    public void createListNation(){
        nations = new ArrayList<>();
        nations.add(new Nation(1,"Viet Nam"));
        nations.add(new Nation(2,"China"));
        nations.add(new Nation(3,"USA"));
    }

    @Test(expected = NationNotFoundException.class)
    public void whenGetNation_withNameNotExist_thenReturnError(){
        when(nationRepository.findNationByName(any(String.class))).thenThrow(NationNotFoundException.class);
        nationService.findNationByName("Invalid Name");
    }

    @Test
    public void whenGetNation_withValidName_thenReturnNation(){
        when(nationRepository.findNationByName("Viet Nam")).thenReturn(java.util.Optional.ofNullable(nation));
        Nation nation_entity = nationService.findNationByName("Viet Nam");
        assertEquals(nation_entity.getId(),nation.getId());
    }

    @Test
    public void whenGetListAllNation_thenReturnListNationDTO(){
        when(nationRepository.findAll()).thenReturn(nations);
        List<NationDTO> nationDTOList = nationService.getListNationDTO();
        assertEquals(nationDTOList.size(),nations.size());
    }
}

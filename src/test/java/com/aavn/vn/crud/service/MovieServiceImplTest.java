package com.aavn.vn.crud.service;

import com.aavn.vn.crud.CrudApplicationTests;
import com.aavn.vn.crud.entity.Category;
import com.aavn.vn.crud.entity.Movie;
import com.aavn.vn.crud.entity.Nation;
import com.aavn.vn.crud.exception.MovieIsInvalidException;
import com.aavn.vn.crud.exception.WrongFilterCriteriaException;
import com.aavn.vn.crud.exception.YearNotValidException;
import com.aavn.vn.crud.model.MovieCriteria;
import com.aavn.vn.crud.model.MovieDTO;
import com.aavn.vn.crud.repository.MovieRepository;
import com.aavn.vn.crud.service.serviceImpl.CategoryServiceImpl;
import com.aavn.vn.crud.service.serviceImpl.MovieServiceImpl;
import com.aavn.vn.crud.service.serviceImpl.NationServiceImpl;
import com.aavn.vn.crud.utility.SaveImage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = CrudApplicationTests.class)
public class MovieServiceImplTest {

    @InjectMocks
    private MovieServiceImpl movieService;

    @Mock
    private NationServiceImpl nationService;

    @Mock
    private CategoryServiceImpl categoryService;

    @Mock
    private MovieRepository movieRepository;

    @Mock
    private SaveImage saveImage;

    @Mock
    private BindingResult result;

    private static MovieCriteria movieCriteria;
    private static List<Movie> movies;
    private static MovieDTO valid_movie;
    private static MovieDTO invalid_movie;
    private static Movie movie;
    private static Nation nation;
    private static Category category;


    @Before
    public void createMovieCriteriaDefault(){
        movieCriteria = new MovieCriteria();
        movieCriteria.setYear("year:2000");
        movieCriteria.setCategory("category:1");
        movieCriteria.setNation("nation:1");
    }

    @Before
    public void createListMoviesDefault(){
        movies = new ArrayList<>();
        movies.add(new Movie(1, "abc", "xyz", 120, 2000, "abc", "abc"));
        movies.add(new Movie(2, "abc", "xyz", 120, 2000, "abc", "abc"));
        movies.add(new Movie(3, "abc", "xyz", 120, 2000, "abc", "abc"));
    }

    @Before
    public void createMovieDTO(){
        valid_movie = new MovieDTO(1,"thumbnail", "title", 120, 2000, "director", "description", "category","nation");
        invalid_movie = valid_movie;
    }

    @Before
    public void createNation(){
        nation = new Nation(1,"Viet Nam");
    }

    @Before
    public void createCategory(){
        category = new Category(1,"Action");
    }

    @Before
    public void createMovie(){
        movie = new Movie(1, "thumbnail", "title", 120, 2000, "director", "description");
    }

    private void prepareForCreateMovie(){
        when(nationService.findNationByName(any(String.class))).thenReturn(nation);
        when(categoryService.findCategoryByName(any(String.class))).thenReturn(category);
        when(saveImage.saveImage(any(String.class), any(String.class))).thenReturn("thumbnail");
        when(movieRepository.save(any(Movie.class))).thenReturn(movie);
    }

    @Test
    public void whenCreateMovie_withValidInfomation_thenReturnStatusOK(){
        prepareForCreateMovie();
        when(result.hasErrors()).thenReturn(false);
        movieService.createMovie(valid_movie, result);
        assertEquals(movie.getId(),valid_movie.getId());
    }

    @Test(expected = YearNotValidException.class)
    public void whenCreateMovie_withInvalidYear_thenReturnStatusOK(){
        prepareForCreateMovie();
        when(result.hasErrors()).thenReturn(false);
        invalid_movie.setYear(2050);
        movieService.createMovie(invalid_movie, result);
        createMovieDTO();
    }

    @Test(expected = MovieIsInvalidException.class)
    public void whenCreateMovie_withInvalidValidation_thenThrowMovieIsInvalidException(){
        prepareForCreateMovie();
        when(result.hasErrors()).thenReturn(true);
        movieService.createMovie(invalid_movie, result);
    }

    @Test
    public void whenGetMovies_thenRetrieveListMovieIsCorrect() {
        when(movieRepository.findAll()).thenReturn(movies);
        List<Movie> movies = movieService.listMovies();
        assertEquals("Size is equal to 3",3,movies.size());
    }

    @Test
    public void whenGetMovies_thenRetrieveListMovie_IsEmpty() {
        when(movieRepository.findAll()).thenReturn(Collections.emptyList());
        List<Movie> movies = movieService.listMovies();
        assertEquals("Size is equal to 0",0, movies.size());
    }

    @Test
    public void whenFilterMovies_withMovieCriteria_thenReturnListFilteredMovies(){
        when(movieRepository.findAll(any(Specification.class))).thenReturn(movies);
        List<Movie> movies = movieService.findMovieBySpecification(movieService.getSpecification(movieCriteria));
        assertEquals(3, movies.size());
    }

    @Test(expected = WrongFilterCriteriaException.class)
    public void whenFilterMovies_withCriteriaIsInvalid_thenThrowWrongFilterCriteriaException(){
        when(movieRepository.findAll(any(Specification.class))).thenThrow(WrongFilterCriteriaException.class);
        movieService.findMovieBySpecification(movieService.getSpecification(movieCriteria));
    }

    @Test(expected = WrongFilterCriteriaException.class)
    public void whenGetSpecification_withNationCriteriaIsInvalid_thenThrowWrongFilterCriteriaException(){
        createMovieCriteriaDefault();
        movieCriteria.setNation("nation:abc");
        movieService.getSpecification(movieCriteria);
    }

    @Test(expected = WrongFilterCriteriaException.class)
    public void whenGetSpecification_withCategoryCriteriaIsInvalid_thenThrowWrongFilterCriteriaException(){
        createMovieCriteriaDefault();
        movieCriteria.setNation("category:abc");
        movieService.getSpecification(movieCriteria);
    }


    @Test

    public void whenGetMovieDetail_thenRetrieveMovieIsCorrectWithId(){
        int id = 1;
        when(movieRepository.findById(id)).thenReturn(movies.get(id-1));
        int movieId = movieService.getMovieDetail(id).getId();
        assertEquals(id,movieId);
    }

    @Test
    public void whenDeleteMovies_withMovieById_thenDeleteMovie(){
        assertEquals(true, movieService.deleteMovieById(1));
    }

    @Test
    public void whenUpdateMovie_withValidMovie_thenReturnUpadtedMovie(){
        when(movieRepository.findMoviesEntityById(any(Integer.class))).thenReturn(movie);
        movieService.updateMovie(valid_movie);
        verify(movieRepository).save(any(Movie.class));
    }
    @Test(expected = YearNotValidException.class)
    public void whenUpdateMovie_withNullDetail_thenThrowNullMessageException(){
        when(movieRepository.findMoviesEntityById(any(Integer.class))).thenReturn(movie);
        invalid_movie.setYear(2078);
        movieService.updateMovie(invalid_movie);
    }
}


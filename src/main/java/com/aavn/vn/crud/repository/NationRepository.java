package com.aavn.vn.crud.repository;

import com.aavn.vn.crud.entity.Nation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface NationRepository extends JpaRepository<Nation,Integer> {
    List<Nation> findAll();
    Optional<Nation> findNationByName(String name);

}

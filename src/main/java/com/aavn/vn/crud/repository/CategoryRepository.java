package com.aavn.vn.crud.repository;

import com.aavn.vn.crud.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {
    List<Category> findAll();
    Optional<Category> findCategoryByName(String name);
}

package com.aavn.vn.crud.repository;

import com.aavn.vn.crud.entity.Movie;
import com.aavn.vn.crud.entity.Nation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer>, JpaSpecificationExecutor<Movie> {
    Movie findById(int id);
    List<Movie> findAll();
    Movie findMoviesEntityById(int  id);
}

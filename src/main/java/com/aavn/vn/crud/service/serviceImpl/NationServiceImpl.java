package com.aavn.vn.crud.service.serviceImpl;

import com.aavn.vn.crud.constant.ErrorMessageConstants;
import com.aavn.vn.crud.entity.Nation;
import com.aavn.vn.crud.exception.NationNotFoundException;
import com.aavn.vn.crud.model.NationDTO;
import com.aavn.vn.crud.repository.NationRepository;
import com.aavn.vn.crud.service.NationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NationServiceImpl implements NationService {
    @Autowired
    NationRepository nationRepository;

    @Override
    public List<Nation> getAllNationEntities() {
        return nationRepository.findAll();
    }

    @Override
    public List<NationDTO> getListNationDTO() {
        List<Nation> nationEntities = getAllNationEntities();
        return parseListNationEntitiesToListNationDTOs(nationEntities);
    }

    @Override
    public Nation findNationByName(String nation) {
        return nationRepository.findNationByName(nation)
                .orElseThrow(()->new NationNotFoundException(ErrorMessageConstants.NATION_NOT_FOUND));
    }

    private List<NationDTO> parseListNationEntitiesToListNationDTOs(List<Nation> nationEntities) {
        List<NationDTO> nationDTOS = new ArrayList<>();
        for (Nation nationEntity : nationEntities){
            nationDTOS.add(parseNationEntityToNationDTO(nationEntity));
        }
        return nationDTOS;
    }

    private NationDTO parseNationEntityToNationDTO(Nation nationEntity) {
        NationDTO nationDTO = new NationDTO();
        nationDTO.setId(nationEntity.getId());
        nationDTO.setName(nationEntity.getName());
        return nationDTO;
    }
}

package com.aavn.vn.crud.service;

import com.aavn.vn.crud.entity.Category;
import com.aavn.vn.crud.model.CategoryDTO;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategoryEntities();
    List<CategoryDTO> getListCategoryDTO();
    Category findCategoryByName(String category);
}

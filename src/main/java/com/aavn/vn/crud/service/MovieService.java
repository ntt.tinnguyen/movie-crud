package com.aavn.vn.crud.service;

import com.aavn.vn.crud.entity.Movie;
import com.aavn.vn.crud.model.*;
import org.springframework.data.jpa.domain.Specification;
import com.aavn.vn.crud.model.MovieDTO;
import org.springframework.validation.BindingResult;

import java.util.List;

public interface MovieService {
    List<Movie> findMovieBySpecification(Specification specification);
    Specification<Movie> getSpecification(MovieCriteria movieCriteria);
    void createMovie(MovieDTO movieDTO, BindingResult result);
    List<Movie> listMovies();
    Movie getMovieDetail(int id);
    boolean deleteMovieById(int id);
    void updateMovie(MovieDTO movieDTO);
}

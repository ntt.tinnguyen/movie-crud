package com.aavn.vn.crud.service;

import com.aavn.vn.crud.entity.Nation;
import com.aavn.vn.crud.model.NationDTO;

import java.util.List;

public interface NationService {
    List<Nation> getAllNationEntities();
    List<NationDTO> getListNationDTO();
    Nation findNationByName(String nation);
}

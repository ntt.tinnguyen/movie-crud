package com.aavn.vn.crud.service.serviceImpl;

import com.aavn.vn.crud.constant.ErrorMessageConstants;
import com.aavn.vn.crud.entity.Category;
import com.aavn.vn.crud.entity.Movie;
import com.aavn.vn.crud.entity.Nation;
import com.aavn.vn.crud.exception.*;
import com.aavn.vn.crud.model.MovieCriteria;
import com.aavn.vn.crud.model.MovieDTO;
import com.aavn.vn.crud.repository.MovieRepository;
import com.aavn.vn.crud.service.CategoryService;
import com.aavn.vn.crud.service.MovieService;
import com.aavn.vn.crud.service.NationService;
import com.aavn.vn.crud.specification.MovieSpecificationBuilder;
import com.aavn.vn.crud.utility.SaveImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private NationService nationService;

    @Autowired
    private CategoryService categoryService;

    SaveImage saveImage = new SaveImage();

    @Value("${images.path}")
    private String UPLOAD_PATH;

    @Override
    public void createMovie(MovieDTO movieDTO, BindingResult result) {
        if (result.hasErrors())
            throw new MovieIsInvalidException(result);
        Movie movieEntity = parseCreateMovieDTOtoMovieEntity(movieDTO);
        String image = saveImage.saveImage(movieDTO.getThumbnail(), UPLOAD_PATH);
        movieEntity.setThumbnail(image);
        Date current_time = new Date();
        movieEntity.setCreatedAt(current_time);
        movieEntity.setUpdatedAt(current_time);
        movieRepository.save(movieEntity);
    }

    private Movie parseCreateMovieDTOtoMovieEntity(MovieDTO createMovieDTO) throws YearNotValidException {
        Movie movieEntity = new Movie();
        movieEntity.setTitle(createMovieDTO.getTitle());
        movieEntity.setDuration(createMovieDTO.getDuration());
        int year = createMovieDTO.getYear();
        if (checkCreateYearAvailable(year)) {
            movieEntity.setYear(year);
        } else {
            throw new YearNotValidException(ErrorMessageConstants.YEAR_NOT_VALID);
        }
        movieEntity.setDirector(createMovieDTO.getDirector());
        movieEntity.setDescription(createMovieDTO.getDescription());
        Nation nationEntity = nationService.findNationByName(createMovieDTO.getNation());
        movieEntity.setNation(nationEntity);
        Category categoryEntity = categoryService.findCategoryByName(createMovieDTO.getCategory());
        movieEntity.setCategory(categoryEntity);
        return movieEntity;
    }

    private boolean checkCreateYearAvailable(int year) {
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int current_year = localDate.getYear();
        if (year > current_year || year < 1888) {
            return false;
        } else
            return true;
    }

    @Override
    public List<Movie> findMovieBySpecification(Specification specification) throws WrongFilterCriteriaException {
        List<Movie> movies;
        try {
            movies = movieRepository.findAll(specification);
        } catch (Exception e) {
            throw new WrongFilterCriteriaException("Filter criteria is invalid!");
        }
        return Optional.ofNullable(movies).orElseThrow(() -> new MovieNotFoundException(ErrorMessageConstants.MOVIE_NOT_FOUND));
    }

    @Override
    public Specification<Movie> getSpecification(MovieCriteria movieCriteria) throws WrongFilterCriteriaException {
        MovieSpecificationBuilder builder = new MovieSpecificationBuilder();
        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>|#)(\\w+?),");
        String criteria = movieCriteria.getYear() + ","
                + movieCriteria.getNation() + ","
                + movieCriteria.getCategory();

        Matcher matcher = pattern.matcher(criteria + ',');

        while (matcher.find()) {
            if ("category".equals(matcher.group(1))) {
                Category category = new Category();
                try {
                    category.setId(Integer.parseInt(matcher.group(3)));
                } catch (Exception e) {
                    throw new WrongFilterCriteriaException(ErrorMessageConstants.INVALID_FILTER_CRITERIA);
                }
                builder.with(matcher.group(1), matcher.group(2), category);
            } else if ("nation".equals(matcher.group(1))) {
                Nation nation = new Nation();
                try {
                    nation.setId(Integer.parseInt(matcher.group(3)));
                } catch (Exception e) {
                    throw new WrongFilterCriteriaException(ErrorMessageConstants.INVALID_FILTER_CRITERIA);
                }
                builder.with(matcher.group(1), matcher.group(2), nation);
            } else {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }
        }
        return builder.build();
    }

    @Override
    public List<Movie> listMovies() {
        return Optional.ofNullable(movieRepository.findAll())
                .orElseThrow(() -> new MovieNotFoundException(ErrorMessageConstants.MOVIE_NOT_FOUND));
    }

    @Override
    public boolean deleteMovieById (int id){
        movieRepository.deleteById(id);
        return true;
    }

    @Override
    public Movie getMovieDetail ( int idDetail){
        return Optional.ofNullable(movieRepository.findById(idDetail))
                .orElseThrow(()-> new MovieNotFoundException(ErrorMessageConstants.MOVIE_NOT_FOUND));
    }

    @Override
    public void updateMovie(MovieDTO movieDTO) {
        Movie movieEntity = movieRepository.findMoviesEntityById(movieDTO.getId());
        if(movieDTO.getTitle() != null) {
            movieEntity.setTitle(movieDTO.getTitle());
        }
        if(movieDTO.getDirector() != null) {
            movieEntity.setDirector(movieDTO.getDirector());
        }
        if(movieDTO.getDuration() != null) {
            movieEntity.setDuration(movieDTO.getDuration());
        }
        String image = saveImage.saveImage(movieDTO.getThumbnail(), UPLOAD_PATH);
        movieEntity.setThumbnail(image);
        if(movieDTO.getDescription() != null) {
            movieEntity.setDescription(movieDTO.getDescription());
        }
        int year = movieDTO.getYear();
        if (checkCreateYearAvailable(year)) {
            movieEntity.setYear(year);
        } else {
            throw new YearNotValidException(ErrorMessageConstants.YEAR_NOT_VALID);
        }
        Nation nationEntity = nationService.findNationByName(movieDTO.getNation());
        movieEntity.setNation(nationEntity);
        Category categoryEntity = categoryService.findCategoryByName(movieDTO.getCategory());
        movieEntity.setCategory(categoryEntity);
        movieRepository.save(movieEntity);
    }
}

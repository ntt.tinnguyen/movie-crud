package com.aavn.vn.crud.service.serviceImpl;

import com.aavn.vn.crud.constant.ErrorMessageConstants;
import com.aavn.vn.crud.entity.Category;
import com.aavn.vn.crud.exception.CategoryNotFoundException;
import com.aavn.vn.crud.model.CategoryDTO;
import com.aavn.vn.crud.repository.CategoryRepository;
import com.aavn.vn.crud.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategoryEntities() {
        return categoryRepository.findAll();
    }

    @Override
    public List<CategoryDTO> getListCategoryDTO() {
        List<Category> categoryEntities = getAllCategoryEntities();
        return parseListCategoryEntitiesToListCategoryDTOs(categoryEntities);
    }

    @Override
    public Category findCategoryByName(String category) {
        return categoryRepository.findCategoryByName(category)
                .orElseThrow(()-> new CategoryNotFoundException(ErrorMessageConstants.CATEGORY_NOT_FOUND));
    }

    private List<CategoryDTO> parseListCategoryEntitiesToListCategoryDTOs(List<Category> categoryEntities) {
        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        for (Category categoryEntity: categoryEntities) {
            categoryDTOList.add(parseCategoryEntityToCategoryDTO(categoryEntity));
        }
        return categoryDTOList;
    }

    private CategoryDTO parseCategoryEntityToCategoryDTO(Category categoryEntity) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(categoryEntity.getId());
        categoryDTO.setName(categoryEntity.getName());
        return categoryDTO;
    }
}

package com.aavn.vn.crud.entity;

import com.fasterxml.jackson.annotation.JsonFilter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@JsonFilter("filter.Nation")
@Table(name = "nations")
public class Nation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "nation", cascade = CascadeType.ALL)
    private List<Movie> movies;

    private Date createdAt;
    private Date updatedAt;

    public Nation(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Nation() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}

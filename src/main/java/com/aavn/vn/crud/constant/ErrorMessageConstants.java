package com.aavn.vn.crud.constant;

public class ErrorMessageConstants {
    public static String YEAR_NOT_VALID = "Inputed year is not valid!";
    public static String NATION_NOT_FOUND = "Nation can not be found";
    public static String CATEGORY_NOT_FOUND = "Category can not be found!";
    public static String MOVIE_NOT_FOUND = "Movie can not be found.";
    public static String INVALID_FILTER_CRITERIA = "Filter criteria is invalid";
}

package com.aavn.vn.crud.exception;

public class MovieNotFoundException extends RuntimeException{
    private String errorMessage;

    public MovieNotFoundException(){
        super();
    }
    public MovieNotFoundException(String errorMessage){
        this.errorMessage = errorMessage;
    }
    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

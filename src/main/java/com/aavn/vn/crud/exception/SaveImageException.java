package com.aavn.vn.crud.exception;

public class SaveImageException extends RuntimeException{
    private String errorMessage;

    public SaveImageException(){
        super();
    }

    public SaveImageException(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

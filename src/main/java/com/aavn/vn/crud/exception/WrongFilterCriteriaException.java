package com.aavn.vn.crud.exception;

public class WrongFilterCriteriaException extends RuntimeException {
    private String errorMessage;

    public WrongFilterCriteriaException(){
        super();
    }

    public WrongFilterCriteriaException(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

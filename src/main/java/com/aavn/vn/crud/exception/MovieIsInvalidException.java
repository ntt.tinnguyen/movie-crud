package com.aavn.vn.crud.exception;

import org.springframework.validation.BindingResult;

public class MovieIsInvalidException extends RuntimeException {
    private BindingResult result;

    public MovieIsInvalidException(){
        super();
    }

    public MovieIsInvalidException(BindingResult result){
        this.result = result;
    }

    public BindingResult getResult() {
        return result;
    }

    public void setResult(BindingResult result) {
        this.result = result;
    }
}

package com.aavn.vn.crud.exception;

public class YearNotValidException extends RuntimeException{
    private String errorMessage;

    public YearNotValidException(){
        super();
    }

    public YearNotValidException(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}

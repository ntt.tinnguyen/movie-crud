package com.aavn.vn.crud.exception;

public class CategoryNotFoundException extends RuntimeException{
    private String errorMessage;

    public CategoryNotFoundException(){
        super();
    }

    public CategoryNotFoundException(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

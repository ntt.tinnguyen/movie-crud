package com.aavn.vn.crud.exception;

public class NationNotFoundException extends RuntimeException{
    private String errorMessage;

    public NationNotFoundException(){
        super();
    }

    public NationNotFoundException(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

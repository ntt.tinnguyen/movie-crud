package com.aavn.vn.crud.specification;

import com.aavn.vn.crud.entity.Movie;
import com.aavn.vn.crud.model.FilterCriteria;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class MovieSpecification implements Specification<Movie> {

    public MovieSpecification(){
    }

    public MovieSpecification(FilterCriteria filterCriteria){
        this.filterCriteria = filterCriteria;
    }

    private FilterCriteria filterCriteria;

    private static String getContainsLikePattern(String searchTerm) {
        if (searchTerm == null || searchTerm.isEmpty()) {
            return "%";
        }
        else {
            return "%" + searchTerm.toLowerCase() + "%";
        }
    }

    @Nullable
    @Override
    public Predicate toPredicate(Root<Movie> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (filterCriteria.getOperation().equalsIgnoreCase(">")) {
            return criteriaBuilder.greaterThanOrEqualTo(
                    root.get(filterCriteria.getKey()), filterCriteria.getValue().toString());
        }
        else if (filterCriteria.getOperation().equalsIgnoreCase("<")) {
            return criteriaBuilder.lessThanOrEqualTo(
                    root.get(filterCriteria.getKey()), filterCriteria.getValue().toString());
        }else if (filterCriteria.getOperation().equalsIgnoreCase("#")){
            if (filterCriteria.getValue().toString().equals("1990s"))
                return criteriaBuilder.between(
                        root.get(filterCriteria.getKey()), 1990, 1999);
            else if (filterCriteria.getValue().toString().equals("2000s"))
                return criteriaBuilder.between(
                        root.get(filterCriteria.getKey()), 2000, 2009);
        }
        else if (filterCriteria.getOperation().equalsIgnoreCase(":")) {
            if (root.get(filterCriteria.getKey()).getJavaType() == String.class) {
                return criteriaBuilder.like(
                        root.get(filterCriteria.getKey()), "%" + filterCriteria.getValue() + "%");
            } else {
                return criteriaBuilder.equal(root.get(filterCriteria.getKey()), filterCriteria.getValue());
            }
        }
        return null;
    }
}

package com.aavn.vn.crud.specification;

import com.aavn.vn.crud.entity.Movie;
import com.aavn.vn.crud.model.FilterCriteria;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class MovieSpecificationBuilder {
    private final List<FilterCriteria> params;

    public MovieSpecificationBuilder() {
        params = new ArrayList<FilterCriteria>();
    }

    public MovieSpecificationBuilder with(String key, String operation, Object value) {
        params.add(new FilterCriteria(key, operation, value));
        return this;
    }

    public Specification<Movie> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification<Movie>> yearSpecs = new ArrayList<>();
        List<Specification<Movie>> nationSpecs = new ArrayList<>();
        List<Specification<Movie>> categorySpecs = new ArrayList<>();

        for (FilterCriteria param : params) {
            switch (param.getKey()){
                case "year":
                    yearSpecs.add(new MovieSpecification(param));
                    break;
                case "nation":
                    nationSpecs.add(new MovieSpecification(param));
                    break;
                case "category":
                    categorySpecs.add(new MovieSpecification(param));
                    break;
            }
        }

        List<Specification<Movie>> results = new ArrayList<>();
        Specification<Movie> yearResult = !yearSpecs.isEmpty()?yearSpecs.get(0):null;
        Specification<Movie> nationResult = !nationSpecs.isEmpty()?nationSpecs.get(0):null;
        Specification<Movie> categoryResult = !categorySpecs.isEmpty()?categorySpecs.get(0):null;
        for (int i = 1; i < yearSpecs.size(); i++) {
            yearResult = Specification.where(yearResult).or(yearSpecs.get(i));
        }
        for (int i = 1; i < nationSpecs.size(); i++) {
            nationResult = Specification.where(nationResult).or(nationSpecs.get(i));
        }
        for (int i = 1; i < categorySpecs.size(); i++) {
            categoryResult = Specification.where(categoryResult).or(categorySpecs.get(i));
        }
        if (yearResult != null)
            results.add(yearResult);
        if (nationResult != null)
            results.add(nationResult);
        if (categoryResult != null);
            results.add(categoryResult);

        Specification<Movie> finalResult = results.get(0);
        for (int i = 0; i < results.size(); i++) {
            finalResult = Specification.where(finalResult).and(results.get(i));
        }

        return finalResult;
    }
}

package com.aavn.vn.crud.controller;

import com.aavn.vn.crud.exception.CategoryNotFoundException;
import com.aavn.vn.crud.exception.NationNotFoundException;
import com.aavn.vn.crud.exception.SaveImageException;
import com.aavn.vn.crud.exception.YearNotValidException;
import com.aavn.vn.crud.exception.WrongFilterCriteriaException;
import com.aavn.vn.crud.exception.*;
import com.aavn.vn.crud.model.MovieErrorDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@CrossOrigin
@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler(SaveImageException.class)
    public ResponseEntity handleSaveImageException(SaveImageException ex){
        String message = ex.getErrorMessage();
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NationNotFoundException.class)
    public ResponseEntity handleNationNotFound(NationNotFoundException ex){
        String message = ex.getErrorMessage();
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CategoryNotFoundException.class)
    public ResponseEntity handleCategoryNotFound(CategoryNotFoundException ex){
        String message = ex.getErrorMessage();
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(YearNotValidException.class)
    public ResponseEntity handleYearNotValid(YearNotValidException ex){
        String message = ex.getErrorMessage();
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(WrongFilterCriteriaException.class)
    public ResponseEntity handleWrongFilterCriteriaException(WrongFilterCriteriaException ex){
        String message = ex.getErrorMessage();
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex){
        String message = "Request Param is invalid!";
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MovieNotFoundException.class)
    public ResponseEntity handleMovieNotFound(MovieNotFoundException ex){
        String message = ex.getErrorMessage();
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MovieIsInvalidException.class)
    public ResponseEntity handleMovieIsInvalidException(MovieIsInvalidException ex){
        BindingResult resultError = ex.getResult();
        MovieErrorDTO movieErrorDTO = new MovieErrorDTO();
        movieErrorDTO.setTitle(resultError.getFieldError("title") != null ? resultError.getFieldError("title").getDefaultMessage() : "");
        movieErrorDTO.setCategory(resultError.getFieldError("category") != null ? resultError.getFieldError("category").getDefaultMessage() : "");
        movieErrorDTO.setNation(resultError.getFieldError("nation") != null ? resultError.getFieldError("nation").getDefaultMessage() : "");
        movieErrorDTO.setDescription(resultError.getFieldError("discription") != null ? resultError.getFieldError("discription").getDefaultMessage() : "");
        movieErrorDTO.setDirector(resultError.getFieldError("director") != null ? resultError.getFieldError("director").getDefaultMessage() : "");
        movieErrorDTO.setYear(resultError.getFieldError("year") != null ? resultError.getFieldError("year").getDefaultMessage() : "");
        movieErrorDTO.setDuration(resultError.getFieldError("duration") != null ? resultError.getFieldError("duration").getDefaultMessage() : "");
        movieErrorDTO.setThumbnail(resultError.getFieldError("thumbnail") != null ? resultError.getFieldError("thumbnail").getDefaultMessage() : "");
        return new ResponseEntity<>(movieErrorDTO, HttpStatus.BAD_REQUEST);
    }

}

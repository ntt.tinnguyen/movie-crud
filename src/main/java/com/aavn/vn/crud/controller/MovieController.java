package com.aavn.vn.crud.controller;

import com.aavn.vn.crud.entity.Movie;
import com.aavn.vn.crud.model.*;
import com.aavn.vn.crud.service.MovieService;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import com.aavn.vn.crud.model.MovieDTO;
import com.aavn.vn.crud.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
public class MovieController {

    @Autowired
    private MovieService movieService;

    @PostMapping("/movie/filter")
    public ResponseEntity filterMovies(@RequestBody MovieCriteria movieCriteria) {
        Specification<Movie> movieSpecification = movieService.getSpecification(movieCriteria);
        List<Movie> movies = movieService.findMovieBySpecification(movieSpecification);
        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(movies);
        FilterProvider filterProvider = new SimpleFilterProvider()
                .addFilter("filter.Movie", SimpleBeanPropertyFilter.serializeAll())
                .addFilter("filter.Category", SimpleBeanPropertyFilter.filterOutAllExcept("name"))
                .addFilter("filter.Nation", SimpleBeanPropertyFilter.filterOutAllExcept("name"));

        mappingJacksonValue.setFilters(filterProvider);
        return new ResponseEntity(mappingJacksonValue, HttpStatus.OK);
    }

    @PostMapping("/movie/create")
    public ResponseEntity createMovie(@RequestBody @Valid MovieDTO movieDTO, BindingResult result){
        movieService.createMovie(movieDTO, result);
        return new ResponseEntity(HttpStatus.CREATED);
    }


    @GetMapping("/movies")
    public ResponseEntity getMovies() {
        List<Movie> movies = movieService.listMovies();
        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(movies);
        FilterProvider filterProvider = new SimpleFilterProvider()
                .addFilter("filter.Movie", SimpleBeanPropertyFilter.serializeAll())
                    .addFilter("filter.Category", SimpleBeanPropertyFilter.filterOutAllExcept("name"))
                    .addFilter("filter.Nation", SimpleBeanPropertyFilter.filterOutAllExcept("name"));

        mappingJacksonValue.setFilters(filterProvider);
        return new ResponseEntity<>(mappingJacksonValue, HttpStatus.OK);
    }

    @GetMapping("/movie/{id}")
    public ResponseEntity getDetailMovie(@PathVariable(value = "id") int id) {
        Movie movie = movieService.getMovieDetail(id);
        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(movie);
        FilterProvider filterProvider = new SimpleFilterProvider()
                .addFilter("filter.Movie", SimpleBeanPropertyFilter.serializeAll())
                .addFilter("filter.Category", SimpleBeanPropertyFilter.filterOutAllExcept("name"))
                .addFilter("filter.Nation", SimpleBeanPropertyFilter.filterOutAllExcept("name"));
        mappingJacksonValue.setFilters(filterProvider);
        return new ResponseEntity(mappingJacksonValue, HttpStatus.OK);
    }

    @DeleteMapping("/movie/{id}")
    public ResponseEntity deleteMovie(@PathVariable(value = "id") int id) {
        movieService.deleteMovieById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/movies/update")
    public ResponseEntity updateMovie(@RequestBody MovieDTO movieDTO) {
        movieService.updateMovie(movieDTO);
        return new ResponseEntity(HttpStatus.CREATED);

    }
}

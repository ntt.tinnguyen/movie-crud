package com.aavn.vn.crud.controller;

import com.aavn.vn.crud.model.NationDTO;
import com.aavn.vn.crud.service.NationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController()
public class NationController {
    @Autowired
    private NationService nationService;

    @GetMapping("/nations")
    public ResponseEntity getAllNation() {
        List<NationDTO> naitonDTO = nationService.getListNationDTO();
        return new ResponseEntity(naitonDTO, HttpStatus.OK);
    }
}

package com.aavn.vn.crud.controller;

import com.aavn.vn.crud.entity.Category;
import com.aavn.vn.crud.model.CategoryDTO;
import com.aavn.vn.crud.model.NationDTO;
import com.aavn.vn.crud.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController()
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @GetMapping("/categories")
    public ResponseEntity getAllCategory() {
        List<CategoryDTO> categoryDTOList = categoryService.getListCategoryDTO();
        return new ResponseEntity(categoryDTOList, HttpStatus.OK);
    }
}

package com.aavn.vn.crud.utility;

import com.aavn.vn.crud.exception.SaveImageException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.UUID;

@Component
public class SaveImage {
    public String saveImage(String base64Image, String image_path) throws SaveImageException {
        String imageName = UUID.randomUUID().toString() + ".jpg";
        BufferedImage image;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(base64Image);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
            File outputfile = new File(image_path + imageName);
            ImageIO.write(image, "jpg", outputfile);
            return imageName;
        } catch (Exception e) {
            throw new SaveImageException("The image is invalid!");
        }
    }
}

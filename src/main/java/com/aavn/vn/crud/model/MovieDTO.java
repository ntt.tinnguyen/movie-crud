package com.aavn.vn.crud.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotEmpty;

public class MovieDTO {
    private Integer id;

    @NotNull(message = "Thumbnail is required!")
    @NotEmpty(message = "Thumbnail is required!")
    private String thumbnail;

    @NotNull(message = "Title is required!")
    @NotEmpty(message = "Title is required!")
    @Length(max = 255, message = "Maximum length is 255!")
    private String title;

    @NotNull(message = "Duration is required!")
    @Min(value = 1, message = "Duration must greater than 1 minutes.")
    private Integer duration;

    @NotNull(message = "Year is required!")
    @Min(value = 1800, message = "Year is not valid.")
    private Integer year;

    @NotEmpty(message = "Director is required!")
    @NotNull(message = "Director is required!")
    private String director;

    @NotEmpty(message = "Description is required!")
    @NotNull(message = "Description is required!")
    @Length(max = 500, message = "Maximum length is 500!")
    private String description;

    @NotNull(message = "Category is required!")
    private String category;

    @NotNull(message = "Nation is required!")
    private String nation;

    public MovieDTO(Integer id, @NotNull(message = "Thumbnail is required!") @NotEmpty(message = "Thumbnail is required!") String thumbnail, @NotNull(message = "Title is required!") @NotEmpty(message = "Title is required!") String title, @NotNull(message = "Duration is required!") @Min(value = 1, message = "Duration must greater than 1 minutes.") Integer duration, @NotNull(message = "Year is required!") Integer year, @NotNull(message = "Director is required!") String director, @NotNull(message = "Description is required!") String description, @NotNull(message = "Category is required!") String category, @NotNull(message = "Nation is required!") String nation) {
        this.id = id;
        this.thumbnail = thumbnail;
        this.title = title;
        this.duration = duration;
        this.year = year;
        this.director = director;
        this.description = description;
        this.category = category;
        this.nation = nation;
    }

    public MovieDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }
}
